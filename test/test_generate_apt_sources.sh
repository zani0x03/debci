#!/bin/sh

. $(dirname $0)/test_helper.sh

generate() {
  output="$__tmpdir/output.txt"
  debci generate-apt-sources "$@" > $output
}

test_testing_non_free_firmware() {
  generate testing
  assertTrue 'has non-free-firmware' 'grep non-free-firmware $output'
}

test_bookworm_non_free_firmware() {
  generate bookworm
  assertTrue 'has non-free-firmware' 'grep non-free-firmware $output'
}

test_bullseye_non_free_firmware() {
  generate bullseye
  assertFalse 'does not have non-free-firmware' 'grep non-free-firmware $output'
}

test_stretch_non_free_firmware() {
  generate stretch
  assertFalse 'does not have non-free-firmware' 'grep non-free-firmware $output'
}

. shunit2
