require 'debci/app'
require 'debci/package'

module Debci
  class Home < Debci::App
    get '/' do
      @package_prefixes = Debci::Package.prefixes
      erb :index
    end
  end
end
